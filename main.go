package main

import (
	"fmt"
	"time"
)

func compute(value int) {
	for i := 0; i <= value; i++ {
		time.Sleep(time.Second)
		fmt.Println(i)
	}
}

func alphabets() {
	for i := 'a'; i < 'k'; i++ {
		time.Sleep(time.Second)
		fmt.Printf("%c\n", i)
	}
}

func numbers(value int, done chan bool) {
	for i := 0; i < value; i++ {
		time.Sleep(time.Second)
		fmt.Println(i)
	}

	done <- true
}

func main() {
	fmt.Println("Golang Goroutines & Channels")

	// go compute(5)
	// go compute(10)

	done := make(chan bool)
	go numbers(5, done)
	<-done
}
